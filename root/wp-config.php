<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'usbw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.=V>3F!EaL#HZQgpb,Z_[b{E&%vG@2V{<DzpHnw=zYBh7nvCtnI[xJU=@jf$47?B');
define('SECURE_AUTH_KEY',  '#$q4{/}DUp*3FSSoO11YW/PDd+%odZf^V4M6cXlRGi_z_wAxLSFJ1%!{._64HV1o');
define('LOGGED_IN_KEY',    'Vs]*CF<aL90kePgn^G=I9r:QV&sI8;#<yOPsBCgK~;$!Q6!`|YHu!|oMh)ivzRP,');
define('NONCE_KEY',        '[CtQv6e>GhQ,;_V_=#GZ10ryJ`YUD VR8i.v5Fvq1>Kd!;c4I+<SBJ^23qff5;q&');
define('AUTH_SALT',        '[S$w;Rgk*cq%S;0,a3?N);t8dCftKC^D=s`N<R0F>Zp].E!JQ,-t.h!7^D-<_lv,');
define('SECURE_AUTH_SALT', 'c0qfqI|?I|D}N0AHC;+n{4%3Qh|o5FPk;C~PVQeKxi_^NCf<{$hlJ2-x(U)$,6!X');
define('LOGGED_IN_SALT',   'oKs5<_])+]4mI.m8|lvJ(J3l;D^ayMci7k:*(@50DhEt30mv7@>m6Dc6l|^Z2,sU');
define('NONCE_SALT',       '9]}9e;#D0k@s(|qH[Dkz_^_Z[0 @rTd~yAiWsJ;NeM]RKrvlk@K8_Go`GsR+F<;H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
